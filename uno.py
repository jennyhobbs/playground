__author__ = 'jenny'

#print "Hello World!"
#print "Hello Again"
#print "I like typing this."
#print "This is fun."
#print 'Yay! Printing.'
#print "I'd much rather you 'not'."
#print 'I "said" do not touch this.'
print 'I can touch whatever I want carajo'

# A comment, this is so you can read your program later.
#Anything after the # is ignored by python.
print "I could have code like this."
# You can also use a comment to 'disable' or comment out a piece of code:
print "this will run"
#print "this will not run"

print "I will now count my chickens:"
# this line will solve a math problems of how many chickens we have.
print "hens", 25 + 30 / 6
# this line will solve a math problems of how many rooster we have.
print "roosters", 100 - 25 * 3 % 4

# this line is counting the numbers of eggs we have
print "now I will count the eggs:"
print 3 + 2 +1 - 5 +4 % 2 -1 / 4 + 6

print "is it true that 3 + 2 < 5 - 7?"
print 3 + 2 < 5 - 7
print "what is 3 + 2?" , 3 + 2
print "what is 5 - 7?" , 5 - 7
print "oh, that's why it's false."
print "how about some more."
print "is it greater?" , 5 > -2
print "is it greater or equal?", 5 >= -2
print "is it less or equal?" , 5 <= -2
